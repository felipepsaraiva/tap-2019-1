#include <iostream>
#include <vector>

using namespace std;

typedef vector<vector<int>> triangle_t;

int m;

int max_sum(triangle_t& t, int r, int c) {
  for (int r = m - 2; r >= 0; r--)
    for (int c = 0; c <= r; c++)
      t[r][c] += max(t[r + 1][c], t[r + 1][c + 1]);

  return t[0][0];
}

int main() {
  cin >> m;
  triangle_t triangle(m, vector<int>(m, 0));

  int element;
  for (int i = 0; i < m; i++) {
    for (int j = 0; j <= i; j++) {
      cin >> element;
      triangle[i][j] = element;
    }
  }

  cout << max_sum(triangle, 0, 0) << endl;
}
