#include <stdio.h>
#include <iostream>

using namespace std;

int map[25][25];
int rows, columns;

int main() {
  int k, r, c;
  cin >> rows >> columns >> k;

  for (int i = 0; i < k; i++) {
    cin >> r >> c;
    map[r - 1][c - 1] = -1;
  }

  for (r = 0; r < rows; r++) {
    if (map[r][0] == -1)
      break;
    map[r][0] = 1;
  }

  for (c = 1; c < columns; c++) {
    if (map[0][c] == -1)
      break;
    map[0][c] = 1;
  }

  for (r = 1; r < rows; r++) {
    for (c = 1; c < columns; c++) {
      if (map[r][c] == -1)
        continue;
      map[r][c] = max(map[r - 1][c], 0) + max(map[r][c - 1], 0);
    }
  }

  cout << map[rows - 1][columns - 1] << endl;
}
