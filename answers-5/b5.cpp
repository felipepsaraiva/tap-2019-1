#include <iostream>

using namespace std;

int main() {
  int n, carlos, votes;
  cin >> n >> carlos;

  for (int i = 0; i < n - 1; i++) {
    cin >> votes;
    if (votes > carlos) {
      cout << 'N' << endl;
      return 0;
    }
  }

  cout << 'S' << endl;
}
