#include <cmath>
#include <iostream>

using namespace std;

int main() {
  long v, n, total;
  cin >> v >> n;

  total = v * n;

  for (int i = 10; i < 100; i += 10) {
    cout << (int)ceil((double)total * i / 100.0);
    if (i < 90)
      cout << ' ';
  }

  cout << endl;
}
