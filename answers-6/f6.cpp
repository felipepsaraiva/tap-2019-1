#include <algorithm>
#include <iostream>

using namespace std;

int main() {
  string str1, str2;
  getline(cin, str1);
  getline(cin, str2);

  sort(str1.begin(), str1.end(), greater<char>());
  sort(str2.begin(), str2.end(), greater<char>());

  if (str1 > str2)
    cout << "First string is older";
  else if (str1 < str2)
    cout << "First string is younger";
  else
    cout << "The two strings are the same age";

  cout << endl;
}
