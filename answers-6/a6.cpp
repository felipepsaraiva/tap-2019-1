#include <array>
#include <iostream>
#include <queue>
#include <set>

#define ROWS 10
#define COLS 9

using namespace std;

typedef array<array<string, COLS>, ROWS> sheet_t;

struct visits {
  set<string> unique;
  queue<string> q;
};

string to_desc(int row, int col) {
  return string(1, row + 'A') + to_string(col + 1);
}

string get_element(sheet_t& sheet, string desc) {
  return sheet[desc[0] - 'A'][desc[1] - '1'];
}

string set_element(sheet_t& sheet, string desc, string val) {
  sheet[desc[0] - 'A'][desc[1] - '1'] = val;
  return val;
}

int int_value(string element) {
  try {
    return stoi(element);
  } catch (invalid_argument) {
    return -1;
  }
}

string process(sheet_t& sheet, string desc, set<string> visits) {
  if (visits.count(desc))
    return set_element(sheet, desc, "*");

  visits.insert(desc);

  string element = get_element(sheet, desc);
  if (element == "*" || int_value(element) >= 0)
    return element;

  int result = 0;
  do {
    string next_desc = element.substr(0, 2);
    element = element.substr(element[2] == '+' ? 3 : 2);

    int value = int_value(process(sheet, next_desc, visits));

    if (value != -1)
      result += value;
    else
      return set_element(sheet, desc, "*");
  } while (!element.empty());

  return set_element(sheet, desc, to_string(result));
}

int main() {
  string element;
  sheet_t sheet;

  for (int i = 0; i < ROWS; i++) {
    for (int j = 0; j < COLS; j++) {
      cin >> element;
      sheet[i][j] = element;
    }
  }

  for (int i = 0; i < ROWS; i++) {
    for (int j = 0; j < COLS; j++) {
      set<string> visits;
      cout << process(sheet, to_desc(i, j), visits);
      if (j < COLS - 1)
        cout << ' ';
    }

    cout << endl;
  }
}
