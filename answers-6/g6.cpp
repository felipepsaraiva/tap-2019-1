#include <algorithm>
#include <iostream>

using namespace std;

int main() {
  string word;
  getline(cin, word);

  cout << word << " --- ";

  sort(word.begin(), word.end());

  int i;
  for (i = 0; i < word.length(); i += 2)
    if (word[i] != word[i + 1] || i == word.length() - 1 ||
        (i > 0 && word[i] == word[i - 1]))
      break;

  cout << (i >= word.length() ? "" : "not ") << "pair isograms" << endl;
}
