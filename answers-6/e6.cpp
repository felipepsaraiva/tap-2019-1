#include <algorithm>
#include <bitset>
#include <iostream>

using namespace std;

int main() {
  int n, k;
  cin >> n >> k;

  string bin(30, '0');
  bin = bin.substr(0, n);
  for (int i = 0; i < k; i++)
    bin[i] = '1';

  do {
    cout << bin << endl;
  } while (prev_permutation(bin.begin(), bin.end()));
}
