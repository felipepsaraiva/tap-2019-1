#include <iostream>
#include <map>

using namespace std;

bool has_separator(string c) {
  return c.find_first_of(" ,.\n") != string::npos;
}

int main() {
  int n;
  string line, paragraph;

  cin >> n;
  getline(cin, line);  // Remove line-break from first line

  getline(cin, paragraph);
  for (int i = 1; i < n; i++) {
    getline(cin, line);
    paragraph += '\n' + line;
  }

  map<string, int> ngrams;

  for (int i = 0; i < paragraph.length(); i++) {
    string uni = paragraph.substr(i, 1);
    if (has_separator(uni))
      continue;
    ngrams[uni] = (ngrams[uni] ? ++ngrams[uni] : 1);

    string bi = paragraph.substr(i, 2);
    if (has_separator(bi) || bi.length() < 2)
      continue;
    ngrams[bi] = (ngrams[bi] ? ++ngrams[bi] : 1);

    string tri = paragraph.substr(i, 3);
    if (has_separator(tri) || tri.length() < 3)
      continue;
    ngrams[tri] = (ngrams[tri] ? ++ngrams[tri] : 1);
  }

  // string unigram, bigram, trigram;
  map<string, int>::iterator unigram = ngrams.end(), bigram = ngrams.end(),
                             trigram = ngrams.end();
  for (map<string, int>::iterator it = ngrams.begin(); it != ngrams.end();
       it++) {
    switch (it->first.length()) {
      case 1:
        if (unigram == ngrams.end() || it->second > unigram->second ||
            (it->second == unigram->second && it->first < unigram->first))
          unigram = it;
        break;
      case 2:
        if (bigram == ngrams.end() || it->second > bigram->second ||
            (it->second == bigram->second && it->first < bigram->first))
          bigram = it;
        break;
      case 3:
        if (trigram == ngrams.end() || it->second > trigram->second ||
            (it->second == trigram->second && it->first < trigram->first))
          trigram = it;
        break;
    }
  }

  cout << "Unigram: " << unigram->first << endl;
  cout << "Bigram:  " << bigram->first << endl;
  cout << "Trigram: " << trigram->first << endl;
}
