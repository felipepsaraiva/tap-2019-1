#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

typedef vector<pair<string, string>> dict_t;

string encode(string word) {
  for (int i = 0; i < word.length(); i++) {
    if (word[i] <= 'c')
      word[i] = '2';
    else if (word[i] <= 'f')
      word[i] = '3';
    else if (word[i] <= 'i')
      word[i] = '4';
    else if (word[i] <= 'l')
      word[i] = '5';
    else if (word[i] <= 'o')
      word[i] = '6';
    else if (word[i] <= 's')
      word[i] = '7';
    else if (word[i] <= 'v')
      word[i] = '8';
    else
      word[i] = '9';
  }

  return word;
}

int main() {
  int n;
  string word;

  cin >> n;
  getline(cin, word);

  vector<string> dict(n);
  vector<string> dict_encoded(n);
  for (int i = 0; i < n; i++) {
    getline(cin, word);
    dict[i] = word;
    dict_encoded[i] = encode(word);
  }

  string text_str;
  vector<string> text;
  while (cin >> text_str)
    text.push_back(text_str);

  unsigned int possibilities = 1;
  string msg = "";
  for (string word : text) {
    int c = count(dict_encoded.begin(), dict_encoded.end(), word);
    possibilities *= c;

    if (c == 0)
      break;

    msg += dict[find(dict_encoded.begin(), dict_encoded.end(), word) -
                dict_encoded.begin()];
    if (word != text.back())
      msg += ' ';
  }

  if (!possibilities)
    cout << "not a valid text" << endl;
  else if (possibilities == 1)
    cout << msg << endl;
  else
    cout << "there are " << possibilities << " possible messages" << endl;
}
