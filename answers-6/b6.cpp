#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

// Using suffix array and longest common prefix
// https://www.geeksforgeeks.org/count-distinct-substrings-string-using-suffix-array/

typedef vector<int> vi;

struct suffix {
  int index;
  string str;
};

bool compare_suffixes(suffix a, suffix b) {
  return a.str < b.str;
}

vi build_suffix_array(string str) {
  vector<suffix> suffixes(str.length());
  for (int i = 0; i < str.length(); i++) {
    suffixes[i].index = i;
    suffixes[i].str = str.substr(i);
  }

  sort(suffixes.begin(), suffixes.end(), compare_suffixes);

  vi suffix_array;
  for (auto a : suffixes)
    suffix_array.push_back(a.index);

  return suffix_array;
}

vi build_lcp_array(string str, vi suffix_array) {
  int size = suffix_array.size();
  vi lcp(size, 0);

  vi inv_suffix_array(size, 0);
  for (int i = 0; i < size; i++)
    inv_suffix_array[suffix_array[i]] = i;

  int k = 0;

  for (int i = 0; i < size; i++) {
    if (inv_suffix_array[i] == size - 1) {
      k = 0;
      continue;
    }

    int j = suffix_array[inv_suffix_array[i] + 1];
    while (i + k < size && j + k < size && str[i + k] == str[j + k])
      k++;

    lcp[inv_suffix_array[i]] = k;
    if (k > 0)
      k--;
  }

  return lcp;
}

int main() {
  string str;
  cin >> str;
  int length = str.length();

  vi suffix_array = build_suffix_array(str);
  vi lcp = build_lcp_array(str, suffix_array);

  int result = length - suffix_array[0];
  for (int i = 1; i < lcp.size(); i++)
    result += (length - suffix_array[i]) - lcp[i - 1];

  result++;
  cout << result << endl;
}
