#!/bin/bash
START=$(date +%s.%N)

cat ${2-input.txt} | ./${1-a.out} | tee exec_out.txt

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Execution took $DIFF seconds"

diff exec_out.txt ${3-output.txt} > /dev/null
OUT_DIFF=$?
if [ "$OUT_DIFF" = '0' ];
then
  echo "Output is correct"
else
  echo "Output is incorrect"
fi
