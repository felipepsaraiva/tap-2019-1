#include <cmath>
#include <iostream>

using namespace std;

int main() {
  int c, a;
  cin >> c >> a;

  float trips = (float)a / (float)(c - 1);

  cout << ceil(trips) << endl;
}
