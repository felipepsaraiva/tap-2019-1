#include <iostream>
#include <vector>

using namespace std;

int main() {
  int n;
  cin >> n;

  vector<int> factorials;
  int i = 1, f = 1;
  while (f < n) {
    factorials.push_back(f);
    i++;
    f *= i;
  }

  i = factorials.size() - 1;
  int quant = 0, sum = 0;
  while (sum != n) {
    for (; i >= 0; i--) {
      if (sum + factorials[i] <= n) {
        quant++;
        sum += factorials[i];
        break;
      }
    }
  }

  cout << quant << endl;
}
