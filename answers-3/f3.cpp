#include <iostream>
#include <cmath>

using namespace std;

int main() {
  int n, d;
  cin >> n >> d;

  int integral = floor((double)n / d);
  n -= (d * integral);

  if (integral > 0) {
    cout << integral;
    if (n > 0) cout << ' ';
  }

  if (n == 0) {
    if (integral == 0) cout << 0;
    cout << endl;
    return 0;
  }

  int gcd = 1;
  for (int i = 2; i <= min(abs(n), abs(d)); i++)
    if (n % i == 0 && d % i == 0)
      gcd = i;

  n /= gcd;
  d /= gcd;

  cout << n << '/' << d << endl;
}
