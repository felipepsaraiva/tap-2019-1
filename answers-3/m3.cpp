#include <iostream>

using namespace std;

int main() {
  int n;
  long plays = 0;
  cin >> n;

  for (int i = 1; i < n; i++)
    for (int j = i + 1; j < n; j++)
      for (int k = j + 1; k < n; k++)
        plays++;

  cout << plays << endl;
}
