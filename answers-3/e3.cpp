#include <cmath>
#include <iostream>

using namespace std;

int main() {
  int x, m;
  cin >> x >> m;

  double fraction = (double)m / (double)x;
  double inverse = ceil(fraction);
  int inverse_i, result = 0;

  while (inverse <= m) {
    inverse_i = inverse;

    if ((x * inverse_i) % m == 1) {
      result = inverse_i;
      break;
    }

    inverse += fraction;
  }

  if (result)
    cout << result;
  else
    cout << "Nao existe este tipo de inteiro.";
  cout << endl;
}
