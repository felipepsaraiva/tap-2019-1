#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int num = 0;

void update_flag(vector<uint8_t>& flags, int index) {
  if (!flags[index]) flags[index] = true;
  else num++;
}

int main() {
  int first, last;
  cin >> first >> last;

  vector<uint8_t> flags(last - first + 1, 0);

  int limit = sqrt(last), result;
  for (int i = 1; i <= limit; i++) {
    result = pow(i, 2);
    if (result <= last && result >= first)
      update_flag(flags, result - first);

    result = pow(i, 3);
    if (result <= last && result >= first)
      update_flag(flags, result - first);
  }

  cout << num << endl;
}
