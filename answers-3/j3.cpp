#include <iostream>

using namespace std;

int main() {
  int year_start, year_end, years_passed;
  cin >> year_start >> year_end;

  cout << "Todas as posicoes mudam no ano " << year_start << "." << endl;

  for (int i = year_start + 6; i <= year_end; i++) {
    years_passed = i - year_start;
    if (years_passed % 2 == 0 && years_passed % 3 == 0 &&
        years_passed % 4 == 0 && years_passed % 5 == 0)
      cout << "Todas as posicoes mudam no ano " << i << "." << endl;
  }
}
