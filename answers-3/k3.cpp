#include <iostream>

using namespace std;

string subtract_str(string num, char sub) {
  int index = num.length() - 1;

  num[index] = num[index] - (sub - '0');
  while (num[index] < '0' && index >= 0) {
    num[index] = num[index] + 10;
    index--;
    num[index] = num[index] - 1;
  }

  return num.substr(num.find_first_not_of('0'));
}

int main() {
  string original, num;
  char last;

  getline(cin, original);
  cout << original << endl;

  num = original;

  while (num.length() > 2) {
    last = num.back();
    num.pop_back();
    num = subtract_str(num, last);
    cout << num << endl;
  }

  cout << "O numero " << original;
  cout << (stoi(num) % 11 == 0 ? "" : " nao");
  cout << " eh divisivel por 11." << endl;
}
