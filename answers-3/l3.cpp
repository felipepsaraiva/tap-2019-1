#include <iostream>
#include <cmath>

using namespace std;

typedef unsigned int ui;

void print_round(ui r, ui undefeated, ui defeated_once, ui eliminated) {
  cout << "Rodada " << r << ": ";
  cout << undefeated << " invicta" << (undefeated > 1 ? "s" : "") << ", ";
  cout << defeated_once << " uma derrota, ";
  cout << eliminated << " eliminada" << (eliminated > 1 ? "s" : "") << "."
       << endl;
}

int main() {
  ui n, r, undefeated, defeated_once, eliminated;
  cin >> n;

  r = 0;
  undefeated = n;
  defeated_once = 0;
  eliminated = 0;

  print_round(r, undefeated, defeated_once, eliminated);

  while (undefeated > 0 || defeated_once > 1) {
    if (undefeated == 1 && defeated_once == 1) {
      undefeated = 0;
      defeated_once = 2;
    } else {
      eliminated += defeated_once / 2;
      defeated_once = ceil(defeated_once / 2.);

      if (undefeated > 1) {
        defeated_once += undefeated / 2;
        undefeated = ceil(undefeated / 2.);
      }
    }

    r++;
    print_round(r, undefeated, defeated_once, eliminated);
  }

  cout << "Total: " << r << " rodadas." << endl;
}
