#include <iostream>

using namespace std;

int main() {
  int num, sum = 1;
  cin >> num;

  for (int i = 2; i <= num/2; i++) {
    if (num % i == 0)
      sum += i;
  }

  cout << num << " eh um numero ";
  if (sum == num) cout << "perfeito";
  else if (sum > num) cout << "abundante";
  else cout << "deficiente";
  cout << '.' << endl;
}
