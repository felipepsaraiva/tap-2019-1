#include <cmath>
#include <iostream>
#include <unordered_set>

using namespace std;

typedef long long ll;

// https://www.geeksforgeeks.org/primitive-root-of-a-prime-number-n-modulo-n/

int power_mod(ll x, unsigned int y, ll p) {
  int res = 1;

  x = x % p;

  while (y > 0) {
    if (y & 1)
      res = (res * x) % p;

    y = y >> 1;
    x = (x * x) % p;
  }

  return res;
}

void prime_factors(unordered_set<ll>& s, ll n) {
  if (n == 0)
    return;

  while (n % 2 == 0) {
    s.insert(2);
    n = n / 2;
  }

  for (int i = 3; i <= sqrt(n); i = i + 2) {
    while (n % i == 0) {
      s.insert(i);
      n = n / i;
    }
  }

  if (n > 2)
    s.insert(n);
}

int main() {
  ll p, n, r;
  bool is_primitive;
  unordered_set<ll> s;

  cin >> p;
  ll phi = p - 1;
  prime_factors(s, phi);

  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> r;

    is_primitive = true;
    for (auto it = s.begin(); it != s.end(); it++) {
      if (power_mod(r, phi / (*it), p) == 1) {
        is_primitive = false;
        break;
      }
    }

    cout << (is_primitive ? "SIM" : "NAO") << endl;
  }
}
