#include <iostream>
#include <bitset>
#include <cmath>

using namespace std;

int main() {
  int n;
  long seq_num = 0;
  cin >> n;

  bitset<16> bs(n);
  for (int i = 0; i < 16; i++) {
    if (bs.test(i))
      seq_num += pow(5, i + 1);
  }

  cout << seq_num << endl;
}
