#include <cmath>
#include <iostream>

using namespace std;

int main() {
  int num, half, sum, diff;
  cin >> num;

  for (int i = 1; i < num; i++) {
    if (num % i != 0)
      continue;

    for (int j = i + 1; j <= num; j++) {
      if (num % j != 0 || i * j != num)
        continue;

      sum = i + j;
      diff = abs(i - j);

      for (int k = 1; k < num; k++) {
        if (k == i || k == j || num % k != 0)
          continue;

        for (int l = k + 1; l < num; l++) {
          if (l == i || l == j || num % l != 0 || k * l != num)
            continue;

          if (sum == abs(k - l) || diff == k + l) {
            cout << num << " eh desagradavel." << endl;
            return 0;
          }
        }
      }
    }
  }

  cout << num << " nao eh desagradavel." << endl;
}
