#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

// Using segment tree
// https://www.geeksforgeeks.org/segment-tree-set-1-sum-of-given-range/

struct seq_element {
  int val, st_index;
};

struct tree_node {
  int val, seq_index;
};

typedef vector<seq_element> seq_t;
typedef vector<tree_node> st_t;

tree_node invalid;

int get_mid(int s, int e) {
  return s + (e - s) / 2;
}

int p_index(int i) {
  return (i - 1) / 2;
}

int lc_index(int i) {
  return 2 * i + 1;
}

int rc_index(int i) {
  return 2 * i + 2;
}

tree_node construct_seg_tree(st_t& st, int i, seq_t& seq, int s, int e) {
  if (s == e) {
    st[i].val = seq[s].val;
    st[i].seq_index = s;
    seq[s].st_index = i;
    return st[i];
  }

  int mid = get_mid(s, e);
  tree_node l = construct_seg_tree(st, lc_index(i), seq, s, mid);
  tree_node r = construct_seg_tree(st, rc_index(i), seq, mid + 1, e);
  st[i] = (l.val > r.val ? l : r);
  return st[i];
}

void update_tree(st_t& st, int i, seq_element& e) {
  tree_node l = st[lc_index(i)];
  tree_node r = st[rc_index(i)];
  st[i] = (l.val > r.val ? l : r);
  if (i > 0)
    update_tree(st, p_index(i), e);
}

tree_node
largest_element(st_t& st, int i, int ss, int se, int qs, int qe, int ignore) {
  if (ss >= qs && se <= qe && st[i].seq_index != ignore)
    return st[i];

  if (ss == se && st[i].seq_index == ignore)
    return invalid;

  if (se < qs || ss > qe)
    return invalid;

  int mid = get_mid(ss, se);
  tree_node l = largest_element(st, lc_index(i), ss, mid, qs, qe, ignore);
  tree_node r = largest_element(st, rc_index(i), mid + 1, se, qs, qe, ignore);
  return (l.val > r.val ? l : r);
}

int main() {
  int n, q, x, y, tmp;
  char cmd;

  cin >> n;

  int height, max_size;
  height = ceil(log2(n));
  max_size = 2 * pow(2, height) - 1;

  invalid.val = -1;
  invalid.seq_index = -1;

  seq_t seq(n);
  st_t st(max_size, invalid);

  for (int i = 0; i < n; i++) {
    cin >> tmp;
    seq[i].val = tmp;
  }

  construct_seg_tree(st, 0, seq, 0, n - 1);

  cin >> q;
  for (int i = 0; i < q; i++) {
    cin >> cmd >> x >> y;
    x--;

    if (cmd == 'A') {
      seq[x].val = y;
      st[seq[x].st_index].val = y;
      update_tree(st, p_index(seq[x].st_index), seq[x]);
      continue;
    }

    y--;
    tree_node max1 = largest_element(st, 0, 0, n - 1, x, y, -1);
    tree_node max2 = largest_element(st, 0, 0, n - 1, x, y, max1.seq_index);
    cout << max1.val + max2.val << endl;
  }
}
