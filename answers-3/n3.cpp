#include <iostream>

using namespace std;

int main() {
  int n;
  cin >> n;

  int cost = 0;

  while (n != 1) {
    for (int i = 2; i <= n; i++) {
      if (n % i == 0) {
        cost = cost + (n - (n / i)) / (n / i);
        n = n - (n / i);
        break;
      }
    }
  };

  cout << cost << endl;
}
