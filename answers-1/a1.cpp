#include <stdio.h>

void print_repeat(int num, char c) {
  for (int i = 0; i < num; i++)
    printf("%c", c);
}

void print_box(int size) {
  for (int i = 0; i < size; i++) {
    if (i == 0 || i == size-1) {
      print_repeat(size, '#');
    } else {
      printf("#");
      print_repeat(size - 2, 'J');
      printf("#");
    }

    printf("\n");
  }
}

int main() {
  int size;
  scanf("%d", &size);
  print_box(size);
  return 0;
}