#include <iostream>
#include <vector>

using namespace std;

typedef vector<int> gauntlet;

int to_index(int col_size, int row, int col) {
  return (row * col_size) + col;
}

int max_score(gauntlet& g, int rows, int cols, int row, int col) {
  if (row >= rows || col >= cols)
    return -1;

  int self_score = g.at(to_index(cols, row, col));

  if (row == rows-1 && col == cols-1)
    return self_score;

  int max_right = max_score(g, rows, cols, row, col + 1);
  int max_bottom = max_score(g, rows, cols, row + 1, col);

  if (max_right > max_bottom)
    return self_score + max_right;
  else
    return self_score + max_bottom;
}

int main() {
  int rows, cols, val;
  cin >> rows >> cols;

  gauntlet g(to_index(cols, rows-1, cols-1) + 1);

  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      cin >> val;
      g[to_index(cols, i, j)] = val;
    }
  }

  cout << max_score(g, rows, cols, 0, 0) << endl;
}
