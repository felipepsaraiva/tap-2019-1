#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
  float value;
  string ops;

  cin >> value;
  getline(cin, ops);

  for (int j = 0; j < ops.length(); j++) {
    switch (ops[j]) {
      case '@':
        value *= 3;
        break;

      case '%':
        value += 5;
        break;

      case '#':
        value -= 7;
        break;
    }
  }

  printf("%.2f\n", value);

  return 0;
}