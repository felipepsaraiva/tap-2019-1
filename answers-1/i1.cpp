#include <iostream>
#include <vector>

using namespace std;

typedef vector<string> snowflake;

bool is_horizontally_symmetric(snowflake& s) {
  int size = s.size(), test_size = size / 2;
  for (int i = 0; i < test_size; i++)
    if (s[i] != s[size - 1 - i]) return false;
  return true;
}

bool is_vertically_symmetric(snowflake& s) {
  for (int i = 0; i < s.size(); i++) {
    string line = s[i];
    int length = line.length(), test_length = length / 2;

    for (int j = 0; j < test_length; j++)
      if (line[j] != line[length - 1 - j]) return false;
  }

  return true;
}

int main() {
  int size;
  cin >> size;

  string line;
  snowflake s;
  getline(cin, line); // Remove line break from size

  for (int i = 0; i < size; i++) {
    getline(cin, line);
    s.push_back(line);
  }

  bool h = is_horizontally_symmetric(s), v = is_vertically_symmetric(s);
  if (h && v) cout << "Magnificent";
  else if (h) cout << "Beautiful";
  else if (v) cout << "Graceful";
  else cout << "Useless";

  cout << endl;
}
