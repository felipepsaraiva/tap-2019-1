#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  int i, j, k;
  string number;
  getline(cin, number);

  int size = number.length();
  for (i = size-1; i > 0; i--)
    if (number[i] > number[i-1])
      break;

  if (i == 0) {
    cout << "USELESS" << endl;
    return EXIT_SUCCESS;
  }

  k = i - 1;
  for (j = i+1; j < size; j++)
    if (number[j] > number[k] && number[j] < number[i])
      i = j;

  char aux = number[i];
  number[i] = number[k];
  number[k] = aux;

  sort(number.begin() + k + 1, number.end());
  cout << number << endl;
}
