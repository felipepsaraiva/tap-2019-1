#include <iostream>
#include <array>
#include <vector>
#include <cmath>

#define X_AXIS 0
#define Y_AXIS 1

using namespace std;

typedef array<int, 2> tree;

struct path {
  int axis, ort_axis, pos;
};

int distance_to_path(path p, tree t) {
  return t[p.ort_axis] - p.pos;
}

bool mask_visible(path p, vector<tree>& trees, vector<bool>& visible_mask) {
  int num_trees = trees.size();
  vector<bool> mask(num_trees, true);

  for (int i = 0; i < num_trees; i++) {
    tree base = trees[i];
    int base_d = distance_to_path(p, base);

    for (int j = 0; j < num_trees; j++) {
      if (j == i) continue;
      if (base[p.axis] != trees[j][p.axis]) continue;

      int comparing_d = distance_to_path(p, trees[j]);

      if (base_d * comparing_d > 0 && abs(base_d) > abs(comparing_d)) {
        mask[i] = false;
        break;
      }
    }

    if (mask[i])
      visible_mask[i] = true;
  }
}

int main() {
  int num_trees, num_paths, val;
  cin >> num_trees >> num_paths;

  vector<tree> trees(num_trees);
  vector<bool> visible_mask(num_trees, false);

  // Read trees
  for (int i = 0; i < num_trees; i++) {
    tree point;

    cin >> val;
    point[X_AXIS] = val;

    cin >> val;
    point[Y_AXIS] = val;

    trees[i] = point;
  }

  string path_str;
  getline(cin, path_str);  // Remove line from last tree

  // Read paths
  for (int i = 0; i < num_paths; i++) {
    path p;
    getline(cin, path_str);

    p.axis = (path_str[0] == 'x' ? Y_AXIS : X_AXIS);
    p.ort_axis = (p.axis == X_AXIS ? Y_AXIS : X_AXIS);
    p.pos = stoi(path_str.substr(2, path_str.length()));

    mask_visible(p, trees, visible_mask);
  }

  int count = 0;
  for (auto v: visible_mask)
    if (v)
      count++;

  cout << count << endl;
}
