#include <iostream>
#include <array>

#define SIZE 5

using namespace std;

typedef array<char, SIZE * SIZE> board;

int to_index(int r, int c) {
  return (r * SIZE) + c;
}

bool is_symbol(char s, board b, int r, int c) {
  if (r < 0 || r >= SIZE) return false;
  if (c < 0 || c >= SIZE) return false;

  char bs = b[to_index(r, c)];
  return (bs == s);
}

bool can_win_horizontal(char s, board b, int r, int c) {
  return (
    (
      is_symbol(s, b, r, c - 3)
      && is_symbol(s, b, r, c - 2)
      && is_symbol(s, b, r, c - 1)
    ) || (
      is_symbol(s, b, r, c - 2)
      && is_symbol(s, b, r, c - 1)
      && is_symbol(s, b, r, c + 1)
    ) || (
      is_symbol(s, b, r, c - 1)
      && is_symbol(s, b, r, c + 1)
      && is_symbol(s, b, r, c + 2)
    ) || (
      is_symbol(s, b, r, c + 1)
      && is_symbol(s, b, r, c + 2)
      && is_symbol(s, b, r, c + 3)
    )
  );
}

bool can_win_vertical(char s, board b, int r, int c) {
  return (
    (
      is_symbol(s, b, r - 3, c)
      && is_symbol(s, b, r - 2, c)
      && is_symbol(s, b, r - 1, c)
    ) || (
      is_symbol(s, b, r - 2, c)
      && is_symbol(s, b, r - 1, c)
      && is_symbol(s, b, r + 1, c)
    ) || (
      is_symbol(s, b, r - 1, c)
      && is_symbol(s, b, r + 1, c)
      && is_symbol(s, b, r + 2, c)
    ) || (
      is_symbol(s, b, r + 1, c)
      && is_symbol(s, b, r + 2, c)
      && is_symbol(s, b, r + 3, c)
    )
  );
}

bool can_win_main_diagonal(char s, board b, int r, int c) {
  return (
    (
      is_symbol(s, b, r - 3, c - 3)
      && is_symbol(s, b, r - 2, c - 2)
      && is_symbol(s, b, r - 1, c - 1)
    ) || (
      is_symbol(s, b, r - 2, c - 2)
      && is_symbol(s, b, r - 1, c - 1)
      && is_symbol(s, b, r + 1, c + 1)
    ) || (
      is_symbol(s, b, r - 1, c - 1)
      && is_symbol(s, b, r + 1, c + 1)
      && is_symbol(s, b, r + 2, c + 2)
    ) || (
      is_symbol(s, b, r + 1, c + 1)
      && is_symbol(s, b, r + 2, c + 2)
      && is_symbol(s, b, r + 3, c + 3)
    )
  );
}

bool can_win_anti_diagonal(char s, board b, int r, int c) {
  return (
    (
      is_symbol(s, b, r - 3, c + 3)
      && is_symbol(s, b, r - 2, c + 2)
      && is_symbol(s, b, r - 1, c + 1)
    ) || (
      is_symbol(s, b, r - 2, c + 2)
      && is_symbol(s, b, r - 1, c + 1)
      && is_symbol(s, b, r + 1, c - 1)
    ) || (
      is_symbol(s, b, r - 1, c + 1)
      && is_symbol(s, b, r + 1, c - 1)
      && is_symbol(s, b, r + 2, c - 2)
    ) || (
      is_symbol(s, b, r + 1, c - 1)
      && is_symbol(s, b, r + 2, c - 2)
      && is_symbol(s, b, r + 3, c - 3)
    )
  );
}

bool can_symbol_win(char s, board b, int r, int c) {
  return (
    can_win_horizontal(s, b, r, c)
    || can_win_vertical(s, b, r, c)
    || can_win_main_diagonal(s, b, r, c)
    || can_win_anti_diagonal(s, b, r, c)
  );
}

int main() {
  char c;
  board b;

  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {
      cin >> c;
      b[to_index(i, j)] = c;
    }
  }

  int play = -1;
  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {
      if (b[to_index(i, j)] != '*')
        continue;

      if (!(can_symbol_win('O', b, i, j) || can_symbol_win('X', b, i, j))) {
        play = to_index(i, j) + 1;
        break;
      }
    }

    if (play != -1)
      break;
  }

  cout << play << endl;
}
