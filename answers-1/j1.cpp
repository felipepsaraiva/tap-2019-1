#include <iostream>
#include <array>
#include <vector>
#include <cmath>
#include <cstdio>

using namespace std;

typedef array<int, 2> point;

float distance(point p1, point p2) {
  return sqrt(pow(p1[0] - p2[0], 2) + pow(p1[1] - p2[1], 2));
}

int main() {
  int m;
  point ship, planet;
  vector<point> planets;
  string line;

  cin >> m;
  getline(cin, line); // Remove line break from m

  for (int i = 0; i < m; i++) {
    getline(cin, line);

    for (int j = 0; j < m; j++) {
      if (line[j] == 'p') {
        planet[0] = i;
        planet[1] = j;
        planets.push_back(planet);
      } else if (line[j] == 's') {
        ship[0] = i;
        ship[1] = j;
      }
    }
  }

  planet = planets[0];
  float d = distance(ship, planet);
  for (int i = 1; i < planets.size(); i++) {
    float tmp_d = distance(ship, planets[i]);
    if (tmp_d < d) {
      planet = planets[i];
      d = tmp_d;
    }
  }

  cout << '(' << ship[0] << ',' << ship[1] << "):";
  cout << '(' << planet[0] << ',' << planet[1] << "):";
  printf("%.2f\n", d);
}
