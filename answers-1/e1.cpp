#include <iostream>
#include <vector>

using namespace std;

int main() {
  int n, m, c, val, silence_count = 0, min_val, max_val;
  cin >> n >> m >> c;

  vector<int> volume;
  for (int i = 0; i < n; i++) {
    cin >> val;
    volume.push_back(val);
  }

  for (int i = 0; i <= n - m; i++) {
    min_val = volume[i];
    max_val = volume[i];

    for (int j = 1; j < m; j++) {
      min_val = min(min_val, volume[i + j]);
      max_val = max(max_val, volume[i + j]);
    }

    if (max_val - min_val <= c)
      silence_count++;
  }

  cout << silence_count << endl;
}
