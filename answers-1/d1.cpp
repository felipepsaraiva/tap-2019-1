#include <iostream>
#include <set>
#include <vector>

using namespace std;

bool is_palindrome(string str) {
  int length = str.length(), test_length = length / 2;

  for (int i = 0; i < test_length; i++)
    if (str[i] != str[length - 1 - i])
      return false;
  return true;
}

int main() {
  vector<string> palindromes;
  set<string> palindromes_unique;
  string word, substr;
  getline(cin, word);

  for (int i = 1; i <= word.length(); i++) {
    for (int j = 0; j <= word.length() - i; j++) {
      substr = word.substr(j, i);
      if (is_palindrome(substr) && palindromes_unique.find(substr) == palindromes_unique.end()) {
        palindromes.push_back(substr);
        palindromes_unique.insert(substr);
      }
    }
  }

  cout << palindromes.size() << " -";
  for (auto str: palindromes)
    cout << " \"" << str << '"';
  cout << endl;

  return 0;
}
