#include <iostream>
#include <set>

using namespace std;

int main() {
  char last = 0;
  string str;
  getline(cin, str);

  for (auto letter: str) {
    if (letter != last) {
      last = letter;
      cout << letter;
    }
  }

  cout << endl;
}
