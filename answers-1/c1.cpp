#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
    int height;
    char letter;

    cin >> height;
    cin >> letter;

    for (int i = 0; i < height; i++) {
      for (int j = 0; j <= i; j++)
        cout << letter;
      cout << endl;

      letter++;
      if (letter > 90)
        letter = 65;
    }

    return 0;
}