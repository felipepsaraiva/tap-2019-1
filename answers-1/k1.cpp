#include <iostream>
#include <queue>
#include <vector>

using namespace std;

class FieldElement {
public:
  int row, col, dist;
  FieldElement(int r, int c, int d): row(r), col(c), dist(d) {}
};

int main() {
  string line;
  int size;
  cin >> size;
  getline(cin, line); // Remove line break from size

  FieldElement start(0, 0, 0);
  vector<string> field;
  vector<vector<bool>> blocked;

  vector<bool> tmp_v(size, false);
  for (int i = 0; i < size; i++) {
    getline(cin, line);

    for (int j = 0; j < size; j++) {
      if (line[j] == 's') {
        start.row = i;
        start.col = j;
        tmp_v[j] = true;
      } else if (line[j] == '*') {
        tmp_v[j] = true;
      } else {
        tmp_v[j] = false;
      }
    }

    field.push_back(line);
    blocked.push_back(tmp_v);
  }

  int distance = -1;
  queue<FieldElement> q;
  q.push(start);

  while (!q.empty()) {
    FieldElement e = q.front();
    q.pop();

    if (field[e.row][e.col] == 'd') {
      distance = e.dist;
      break;
    }

    if (e.row - 1 >= 0 && !blocked[e.row - 1][e.col]) {
      q.push(FieldElement(e.row - 1, e.col, e.dist + 1));
      blocked[e.row - 1][e.col] = true;
    }

    if (e.row + 1 < size && !blocked[e.row + 1][e.col]) {
      q.push(FieldElement(e.row + 1, e.col, e.dist + 1));
      blocked[e.row + 1][e.col] = true;
    }

    if (e.col - 1 >= 0 && !blocked[e.row][e.col - 1]) {
      q.push(FieldElement(e.row, e.col - 1, e.dist + 1));
      blocked[e.row][e.col - 1] = true;
    }

    if (e.col + 1 < size && !blocked[e.row][e.col + 1]) {
      q.push(FieldElement(e.row, e.col + 1, e.dist + 1));
      blocked[e.row][e.col + 1] = true;
    }
  }

  cout << distance << endl;
}
