#include <iostream>
#include <bitset>

using namespace std;

int count_char(string str, char c) {
  int count = 0;
  for (auto sc: str)
    if (sc == c)
      count++;
  return count;
}

int main() {
  int num, count_zeros, count_ones;
  cin >> num;

  string binary = bitset<32>(num).to_string();
  int first_one = binary.find_first_of('1');
  binary = binary.substr(first_one, binary.length());

  count_zeros = count_char(binary, '0');
  count_ones = count_char(binary, '1');

  if (count_zeros > count_ones)
    cout << "left";
  else if (count_ones > count_zeros)
    cout << "right";
  else
    cout << "straight";

  cout << endl;
}
