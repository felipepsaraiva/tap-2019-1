#include <iostream>

using namespace std;

int count_forward_spins(char a, char b) {
  int count = 0;

  while (a != b) {
    count++;
    a++;
    if (a > 'Z') a = 'A';
  }

  return count;
}

int count_backward_spins(char a, char b) {
  int count = 0;

  while (a != b) {
    count++;
    a--;
    if (a < 'A') a = 'Z';
  }

  return count;
}

int main() {
  int count = 0, f_count, b_count;
  string s, t;
  getline(cin, s);
  getline(cin, t);

  for (int i = 0; i < s.length(); i++) {
    f_count = count_forward_spins(s[i], t[i]);
    b_count = count_backward_spins(s[i], t[i]);

    count += min(f_count, b_count);
  }

  cout << count << endl;
}
