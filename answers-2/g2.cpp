#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main() {
  int n1, n2, count = 0;
  string n1_str, n2_str;
  cin >> n1;

  do {
    n1_str = to_string(n1);
    sort(n1_str.begin(), n1_str.end(), greater<char>());

    n2_str = n1_str;
    reverse(n2_str.begin(), n2_str.end());

    n1 = stoi(n1_str);
    n2 = stoi(n2_str);
    n1 = n1 - n2;

    count++;
  } while (n1 != 6174);

  cout << count << endl;
}
