#include <iostream>
#include <vector>
#include <array>
#include <cmath>

using namespace std;

typedef array<long, 2> point_t;
typedef vector<vector<long>> cost_t;

int main() {
  long n, val;
  point_t bob, p;
  vector<point_t> shops;
  vector<long> close_time;
  cost_t cost;

  cin >> n;

  // Read Bob location
  cin >> val;
  bob[0] = val;
  cin >> val;
  bob[1] = val;

  // Read shops location
  for (int i = 0; i < n; i++) {
    cin >> val;
    p[0] = val;
    cin >> val;
    p[1] = val;

    shops.push_back(p);
  }

  // Read cost matrix
  for (int i = 0; i <= n; i++) {
    vector<long> line_cost(n + 1);

    for (int j = 0; j <= n; j++)  {
      cin >> val;
      line_cost[j] = val;
    }

    cost.push_back(line_cost);
  }

  // Read time to close
  for (int i = 0; i < n; i++) {
    cin >> val;
    close_time.push_back(val);
  }

  for (int i = 0; i < n; i++) {
    point_t shop = shops[i];
    long dist = abs(bob[0] - shop[0]) + abs(bob[1] - shop[1]);
    dist *= cost[0][i + 1];

    if (dist <= close_time[i]) {
      cout << "SIM" << endl;
      return 0;
    }
  }

  cout << "NAO" << endl;
}
