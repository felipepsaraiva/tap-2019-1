#include <iostream>

using namespace std;

int main() {
  int m, n;
  string s, t;

  cin >> m >> n;
  getline(cin, s); // Remove line-break from m and n input
  getline(cin, s);
  getline(cin, t);

  int index = 0;
  for (int i = 0; i < m; i++) {
    if (s[i] == t[index]) {
      index++;
      if (index >= n)
        break;
    }
  }

  cout << (index == n ? "SIM" : "NAO") << endl;
}
