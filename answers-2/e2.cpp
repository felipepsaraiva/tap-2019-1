#include <iostream>
#include <vector>

using namespace std;

vector<string> steal, not_steal;

bool check_pattern(string& pattern) {
  for (auto s: steal)
    if (s.find(pattern) == string::npos)
      return false;

  for (auto s: not_steal)
    if (s.find(pattern) != string::npos)
      return false;

  return true;
}

int main() {
  int num;
  string str;

  cin >> num;
  getline(cin, str); // Remove line break from num

  for (int i = 0; i < num; i++) {
    getline(cin, str);

    if (str[0] == 'V')
      steal.push_back(str.substr(2));
    else
      not_steal.push_back(str.substr(2));
  }

  str = steal[0];
  int len = str.length();
  for (int i = 0; i < len; i++) {
    for (int j = len - i; j > 0; j--) {
      string pattern = str.substr(i, j);

      if (check_pattern(pattern)) {
        cout << pattern << endl;
        return 0;
      }
    }
  }
}
